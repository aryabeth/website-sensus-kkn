<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_model extends CI_Model{

    public function login_validation($value)
    {
        $sql = "SELECT * from user where username = ? AND password = ?";
        $res = $this->db->query($sql, $value);
        if ($res->num_rows() > 0) {
            return $res->row_array();
        }else{
            return false;
        }
    }

    public function get_hubungan()
    {
        $sql = "SELECT * from hubungan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_agama()
    {
        $sql = "SELECT * from agama";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_penghasilan()
    {
        $sql = "SELECT * from penghasilan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_lahan()
    {
        $sql = "SELECT * from kepemilikan_lahan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_pemasaran()
    {
        $sql = "SELECT * from pemasaran";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function insert_kk($value='')
    {
        $res = $this->db->insert('keluarga', $value);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function get_master_komoditas()
    {
        $sql = "SELECT * from master_komoditas";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    
}
?>