<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DDK - Dusun Munggur</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inputdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>public/js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container">
		    	<div class="col-md-12">
			     	<div class="col-md-8">
			     		
			     	</div>
					<div class="col-md-4">
						<h4 class="pull-right"><i class="glyphicon glyphicon-user">&nbsp;</i><?php echo $this->session->userdata('user_input')['username']; ?> &nbsp;&nbsp;
						<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-success">Logout</a></h4>
					</div>
				</div>
			
		</div>
	</nav>

	<div class="container" >
		<div class="col-md-12 customtitle" style='color:#fff;'>
			<h1>Dashboard Pengisian Data Dasar Keluarga</h1>
		</div>
		<ul class="nav nav-tabs" role="tablist" id="myTab">
			<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data KK</a></li>
			<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Data Anggota Keluarga</a></li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">
				<div class="col-md-12 inputdatawarga">
					<form style="margin: 20px 0px 20px 100px;" class="form-horizontal" role="form" method="post" id="forminputkk" action="<?php echo base_url(); ?>welcome/insert_kk">
						<div class="form-group">			
					    	<label class="control-label col-md-3" >Nama KK </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="nama_kk" name="nama_kk" placeholder="Nama Kepala Keluarga" />
							</div>
						</div>
						<div class="form-group">			
					    	<label class="control-label col-md-3" >Alamat </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="alamat_kk" name="alamat_kk" placeholder="Alamat" />
							</div>
						</div>
						<div class="form-group">			
					        <label class="control-label col-md-3" >RT/RW </label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="rt_kk" name="rt_kk" placeholder="RT" />
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" id="rw_kk" name="rw_kk" placeholder="RW" />
							</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Dusun </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="dusun" name="dusun" placeholder="Dusun" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Desa/Kelurahan </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kelurahan" name="kelurahan" value="Watusigar" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kecamatan </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kecamatan" name="kecamatan" value="Ngawen" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kabupaten </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kabupaten" name="kabupaten" value="Gunung Kidul" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Provinsi </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="provinsi" name="provinsi" value="Yogyakarta" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kepemilikan lahan </label>
								<div class="col-md-6">
									<select class="form-control select" name="kepemilikan_lahan" id="kepemilikan_lahan">
										<?php  
											if (!empty($lahan)) {
												foreach ($lahan as $value) {
													echo '<option value="'.$value['lahan_id'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Komoditas </label>
							<div class="col-md-6">

								<SCRIPT language="javascript">
							        function addRow(tableID) {
							 
							            var table = document.getElementById(tableID);
							 
							            var rowCount = table.rows.length;
							            var row = table.insertRow(rowCount);
							 
							            var colCount = table.rows[0].cells.length;
							 
							            for(var i=0; i<colCount; i++) {
							 
							                var newcell = row.insertCell(i);
							 
							                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
							                //alert(newcell.childNodes);
							                switch(newcell.childNodes[0].type) {
							                    case "text":
							                            newcell.childNodes[0].value = "";
							                            break;
							                    case "checkbox":
							                            newcell.childNodes[0].checked = false;
							                            break;
							                    case "select-one":
							                            newcell.childNodes[0].selectedIndex = 0;
							                            break;
							                }
							            }
							        }
							 
							        function deleteRow(tableID) {
							            try {
							            var table = document.getElementById(tableID);
							            var rowCount = table.rows.length;
							 
							            for(var i=0; i<rowCount; i++) {
							                var row = table.rows[i];
							                var chkbox = row.cells[0].childNodes[0];
							                if(null != chkbox && true == chkbox.checked) {
							                    if(rowCount <= 1) {
							                        alert("Cannot delete all the rows.");
							                        break;
							                    }
							                    table.deleteRow(i);
							                    rowCount--;
							                    i--;
							                }
							            }
							        }catch(e) {
							            alert(e);
							        }
							    }
							 
							    </SCRIPT>

							    <input type="button" class="btn btn-success" value="Tambah Komuditas" onclick="addRow('dataTable')" style="margin-bottom:10px;" />
   				 				<input type="button" class="btn btn-danger" value="Hapus Komoditas" onclick="deleteRow('dataTable')" style="margin-bottom:10px;" />

   				 				<table id="dataTable">

							        <tr style="margin-top: 50px">
							            <td><input type="checkbox" name="chk"/></td>
							            <td>&nbsp;&nbsp;&nbsp;</td>
							            <td>
							                <input type="text" class="form-control" name="komoditas" placeholder="Jenis Komoditas" /></td>
							            </td>
							            <td>&nbsp;&nbsp;&nbsp;</td>
							            <td>
							            	<div class="input-group">	
											  	<input type="text" class="form-control" name="hasil" placeholder="hasil per tahun (kg)">
												<span class="input-group-addon" id="basic-addon1">Kg</span>
											</div>
							            </td>
							        </tr>
							    </table>

							    <!-- <a href="#komoditas_modal" id="komoditas" name="komoditas" data-toggle="modal"><i class="glyphicon glyphicon-plus">&nbsp;Tambah Komoditas</i></a><br><br> -->

								<!-- <table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th  style="text-align:left"> Nama Tanaman </th>
											<th  style="text-align:left" width="40%"> Jumlah per tahun (kg) </th>
											
										</tr>
									</thead>
									<tbody id="addinput">
											<tr>
												<td>Jagung</td>
												<td><input type="text" size="10"></td>			
											</tr>
									</tbody>
								</table> -->
								
								<!-- <input type="text" class="form-control" data-toggle="modal" data-target="#komoditas_modal" id="komoditas" name="komoditas" placeholder="Komoditas" /> -->
							</div>
						</div>

						<!-- submit -->
						<div class="form-group">			
				            	<div class="col-md-9">
				            		<div class="col-md-2 pull-right custombutton">
					            		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					            	</div>
			            		</div>
						</div>
						
					</form>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane " id="profile">
				<div class="col-md-12  inputdatawarga">
					 
					<form style="margin: 20px 0px 20px 100px;" class="form-horizontal" role="form" method="post" id="forminputddk" action="<?php echo base_url(); ?>welcome/inputddk">
					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Kepala Keluarga </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="nama_kepala_keluarga" data-toggle="modal" data-target="#namakk" name="nama_kepala_keluarga" placeholder="Nama Kepala Keluarga" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Lengkap </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Jenis Kelamin </label>
								<div class="col-md-9">
									<div class="radio-list">
										<label>
											<input type="radio" style="margin-left: 0px" name="jenis_kelamin" id="newJenisKelamin" value="L" data-title="Pria"/><span style="margin-left:5px">Pria</span> 
										</label >
										<label style="margin-left: 10px">
											<input type="radio" style="margin-left: 10px" name="jenis_kelamin" id="newJenisKelamin2" value="P" data-title="Wanita"/><span style="margin-left:5px">Wanita</span> 
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Hub. dg Keluarga </label>
								<div class="col-md-3">
									<select class="form-control select isian" name="hubungan" id="hubungan">
										<?php  
											if (!empty($hubungan)) {
												foreach ($hubungan as $value) {
													echo '<option value="'.$value['hub_id'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Tempat Lahir </label>
								<div class="col-md-3">
									<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Kelahiran" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Tanggal Lahir </label>
								<div class="col-md-3">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input type="text" class="form-control" data-provide ="datepicker" name="tgl_lahir" date-date-format="dd/mm/yyyy" placeholder="Tanggal Lahir">
									</div>
									
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Status Nikah </label>
								<div class="col-md-2">
									<select class="form-control select isian" name="status_nikah" id="status_nikah">
										<option value="Kawin" selected>Kawin</option>
										<option value="Belum Kawin">Belum Kawin</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Agama </label>
								<div class="col-md-2">
									<select class="form-control select" name="agama" id="agama">
										<?php  
											if (!empty($agama)) {
												foreach ($agama as $value) {
													echo '<option value="'.$value['agama_id'].'">'.$value['nama_agama'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Gol Darah </label>
								<div class="col-md-3">
									<select class="form-control select" name="gol_darah" id="gol_darah">
										<option value="tidak diketahui" selected>tidak diketahui</option>
										<option value="A" >A</option>
										<option value="AB">AB</option>
										<option value="B">B</option>
										<option value="O">O</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Kewarganegaraan </label>
								<div class="col-md-2">
									<select class="form-control select" name="kewarganegaraan" id="kewarganegaraan">
										<option value="WNI" selected>WNI</option>
										<option value="WNA">WNA</option>
										<option value="DwiKewarganegaraan">Dwi Kewarganegaraan</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Pendidikan Terakhir </label>
								<div class="col-md-2">
									<select class="form-control select" name="pendidikan" id="pendidikan">
										<option value="TK" selected>TK</option>
										<option value="SD">SD</option>
										<option value="SMP">SMP</option>
										<option value="SMA">SMA</option>
										<option value="D3">Diploma</option>
										<option value="S1">Sarjana Strata 1</option>
										<option value="S2">Sarjana Strata 2</option>
										<option value="S3">Sarjana Strata 3</option>
										<option value="SLB">SLB</option>
										<option value="tidak tahu">tidak tahu</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Mata Pencaharian </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="mata_pencaharian" name="mata_pencaharian" placeholder="Mata Pencaharian" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Bapak </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="nama_bapak" name="nama_bapak" placeholder="Nama Bapak/Ibu" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Akta Kelahiran </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="akta_kelahiran" name="akta_kelahiran" placeholder="Akta Kelahiran" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >NIK </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="nik" name="nik" placeholder="Nomor Induk Keluarga" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Akesptor KB </label>
								<div class="col-md-4">
									<select class="form-control select" name="akseptor_kb" id="akseptor_kb">
										<option value="suntik" selected>Kontrasepsi Suntik</option>
										<option value="spiral">Kontrasepsi Spiral</option>
										<option value="kondom">Kontrasepsi Kondom</option>
										<option value="vasektomi">Kontrasepsi Vasektomi</option>
										<option value="tubektomi">Kontrasepsi Tubektomi</option>
										<option value="pil">Kontrasepsi pil</option>
										<option value="alamiah">KB Alamiah/kalender</option>
										<option value="tradisional">Obat Tradisional</option>
										<option value="kba">Alat Kontrasepsi KBA</option>
										<option value="tidak KB">tidak KB</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Cacat </label>
								<div class="col-md-4">
									<input type="text" class="form-control" id="cacat" name="cacat" placeholder="Tuna Rungu, Sumbing, dll..." />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Kepemilikan Rumah </label>
								<div class="col-md-4">
									<select class="form-control select" name="kepemilikan_rumah" id="kepemilikan_rumah">
										<option value="sendiri" selected>Milik Sendiri</option>
										<option value="orangtua">Milik Orangtua</option>
										<option value="keluarga">Milik Keluarga</option>
										<option value="kontrak">Sewa/Kontrak/Pinjam Pakai</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Penghasilan perbulan </label>
								<div class="col-md-4">
									<select class="form-control select" name="penghasilan_perbulan" id="penghasilan_perbulan">
										<?php  
											if (!empty($penghasilan)) {
												foreach ($penghasilan as $value) {
													echo '<option value="'.$value['id'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Pengeluaran perbulan </label>
								<div class="col-md-4">
									<select class="form-control select" name="pengeluaran_perbulan" id="pengeluaran_perbulan">
										<?php  
											if (!empty($penghasilan)) {
												foreach ($penghasilan as $value) {
													echo '<option value="'.$value['id'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Pemerintahan </label>
								<div class="col-md-4">
									<input type="text" class="form-control" id="lembaga" name="lembaga" placeholder="Kepala Desa, Sekdes, dsb..." />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Kemasyarakatan </label>
								<div class="col-md-4">
									<input type="text" class="form-control" id="lembaga_mas" name="lembaga_mas" placeholder="RT, RW, PKK..." />
								</div>
							</div>

							<div class="form-group">			
				            	<div class="col-md-9">
				            		<div class="col-md-2 pull-right custombutton">
					            		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					            	</div>
				            	</div>
							</div>
					</form>
				
				</div>
			</div>			  	
			
		</div>


		<div class="modal fade" id="namakk" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Nama Kepala Keluarga</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="form-group">	
								<div class="col-md-6">
									<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="cari..."/>
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-info">Cari</button>
								</div>
								<br><br>	
							</div>		
							<div style="margin-left:20px; margin-right:20px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKK">
									<thead>
										<tr class="warning">
											<td>Nama Kepala Keluarga</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody id="t_body_merk">
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="modal fade" id="komoditas_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Komoditas</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKomoditas">
									<thead>
										<tr class="warning">
											<td>Nama Komuditas</td>
											<td width="20%">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<?php 
											if (!empty($komoditas)) {
												foreach ($komoditas as $value) {
													echo '<tr>';
														echo '<td>'.$value['jenis'].'</td>';
														echo '<td width="10%" style="text-align:center"><a href="#" class ="addNew"><i class="glyphicon glyphicon-check"></i></a></td>';
													echo '</tr>';
												}
											}
										?>
										
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Selesai</button>
					</div>
				</div>
			</div>
		</div> -->
	</div>
	<br>
	<div style="padding:30px; margin:-20px 100px 0px 100px; text-align:center; font-size: 15pt; color:white;">
		TIM KKN UKDW YOGYAKARTA 2015
	</div>
</body>
</html>